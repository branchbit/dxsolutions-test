<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Item;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @RouteResource("Item")
 */
class RestItemController extends FOSRestController
{

    public function getAction($id)
    {
        $data = $this->getDoctrine()->getRepository('AppBundle:Item')->find($id);
        if (!$data) {
            return New JsonResponse(null, '404');
        }
        $view = $this->view($data, 200);
        return $this->handleView($view);
    }

    public function cgetAction()
    {
        $data = $this->getDoctrine()->getRepository('AppBundle:Item')->findAll();
        if (!$data) {
            return New JsonResponse(null, '404');
        }
        $view = $this->view($data, 200);
        return $this->handleView($view);
    }

    public function postAction(Request $request)
    {
        $data = $request->request->all();
        $serializer = $this->get('jms_serializer');
        $jsonString = $serializer->serialize($data, 'json');
        $newItem = $this->get('jms_serializer')->deserialize($jsonString, Item::class, 'json');

        $errors = $this->get('validator')->validate($newItem);
        if (count($errors) > 0) {
            // todo:make a proper json output for validation errors
            return New JsonResponse($errors, '400');
        }

        // todo:try catch this just to make sure
        $this->getDoctrine()->getManager()->persist($newItem);
        $this->getDoctrine()->getManager()->flush();

        $view = $this->view($newItem, 201);
        return $this->handleView($view);
    }

    public function putAction(Request $request, $id)
    {
        $data = $this->getDoctrine()->getRepository('AppBundle:Item')->find($id);
        if (!$data) {
            return New JsonResponse(null, '404');
        }
        $data = $request->request->all();
        $data['id'] = $id; // todo:put a check in place to make sure these match
        $serializer = $this->get('jms_serializer');
        $entity = $serializer->deserialize($serializer->serialize($data, 'json'), Item::class, 'json');

        $errors = $this->get('validator')->validate($entity);
        if (count($errors) > 0) {
            // todo:make a proper json output for validation errors
            return New JsonResponse($errors, '400');
        }

        // todo:try catch this just to make sure
        $this->getDoctrine()->getManager()->merge($entity);
        $this->getDoctrine()->getManager()->flush();

        $view = $this->view($entity, 200);
        return $this->handleView($view);
    }

    public function deleteAction($id)
    {
        $data = $this->getDoctrine()->getRepository('AppBundle:Item')->find($id);
        if (!$data) {
            return New JsonResponse(null, '404');
        }

        $this->getDoctrine()->getManager()->remove($data);
        $this->getDoctrine()->getManager()->flush();

        $view = $this->view(null, 200);
        return $this->handleView($view);
    }

}
